# About

This is a style that provides useful macros for setup and easy handling of reply letters for journals.

# Install

The style should be installed in your local `texmf` tree. 

For example, you can execute the following

```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab.com:adin/reply.git
```

Or you can extract the files directly into the same local `texmf` directory.

# Usage

To use the example as starting point in your own projects, it is strongly advice not to copy the whole style into your main project. Just pick the `.tex` files in the `example` folder and use them in your own projects. The style repository can be stored safely on the local `texmf` tree.

Another option is just to import the style in your project (through a `\usepackage{reply}` call). And remember to leave the style in its own directory in the local `texmf` tree. 

# Provided macros

## Reply 

- Environment `comment`: It is an environment to typeset the comments from the reviewers. The idea behind it is to put a block of text and use it as context. Then, reply to each individual question by showing it using the `\qa` macro (or `question` and `answer` environments).

- Environment `question`: It is an environment to typeset a given question from the reviewer. Usually it will be after a `comment` and before an `answer` environments.

- Environment `answer`: It is an environmet to typset a given answer to a question from the reviewer. Usually it will be after a `question` environment.

- `\qa{<question>}{<answer>}`: It is a macro to easily combine the `question` and `answer` environments. The two parameters are the `<question>` and `<answer>` text for each part.

- Enrionment `\begin{oldtext}{<section-label>}{<num | range-num>}<text>\end{oldtext}`: It is an environment to typeset a quote from the old manuscript. It uses three parameters:
  - `<section-label>` is the key label used in the old manuscript. If you are using `xcite` package you should use the keyword set to differentiate the labels between the two versions. (See `example/letter.tex` for an example.)
  - `<num | range-num>` is the number or range of numbers (e.g., 3-5) of the paragraph in that section that you are modifying.
  - `<text>` is the quote that you want to typeset.

- Enrionment `\begin{newtext}{<section-label>}{<num | range-num>}<text>\end{oldtext}`: It is an environment to typeset a quote from the revised manuscript. The parameters behave the same as `oldtext` environment.

## Miscelaneous

- `\ellipsis`: Prints an ellipsis into the text `[...]`.

- `\changed{<text>}`: Typesets the `<text>` using `\blue` macro to highlight the changes.

- Latin macros: Several macros are provided to typeset latin abbreviations, for example: `\eg`, `\ie`, `\cf`, `\etal`, `\etc`, and also `\wrt`. And their capital letter version by using a first capital letter in the macro, for example, `\Eg`.

# Example

An example on how to use this style is shown in the `example` folder. To execute it and see the results you can simply execute

```bash
cd ./example
make
```